package ee.ut.math.tvt;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

public class BillRowTest {

  private SaleItem item1;
  
  /**
   * Methods with @Before annotations will be invoked before each test is run.
   */
  @Before
  public void setUp() {
    item1 = new SaleItem("Lauaviin", 3.50); 
  }

  @Test
  public void testRowSumWithZeroQuantity() {
    BillRow r = new BillRow(item1, 0);
    
    assertEquals(r.getRowPrice(), 0.0, 0.0001);
  }

  @Test
  public void testRowSumWithoutDiscount() {
    BillRow r = new BillRow(item1, 2);
    assertEquals(r.getRowPrice(), 7.00, .0001);
  }

  @Test
  public void testRowSumWithDiscount() {
    BillRow r = new BillRow(item1, 1, 20.0);
    assertEquals(r.getRowPrice(), 2.80, .0001);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testRowSumWithInvalidDiscount() {
    BillRow r = new BillRow(item1, 1, -20.0);
  }
  
  // TODO
  public void testRowSumWithNegativeQuantity() {}

  // TODO
  public void testRowSumWithNegativePrice() {}
  
}
