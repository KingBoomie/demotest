package ee.ut.math.tvt;

import org.junit.Before;
import org.junit.Test;

public class BillTest {

    // Bill row costs (reused)
    private static double cost1 = 452.12;
    private static double cost2 = 65.22;
    private static double cost3 = 102.0;
    private static double totalSum = 619.34;

    private Bill bill1;
    
    /**
     * Methods with @Before annotations will be invoked before each test is run.
     */
    @Before
    public void setUp() {
        bill1 = new Bill();
    }

    // TODO
    @Test
    public void testBillWithNoRows() {

    }


    // TODO
    public void testBillWithOneRow() {
    }

    
    // TODO
    public void testBillWithManyRows() {
    }
    
}
